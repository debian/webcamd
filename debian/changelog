webcamd (0.7.6+repack0-2) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Fix day-of-week for changelog entry 0.7.5-1.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 01 Feb 2021 23:03:50 +0000

webcamd (0.7.6+repack0-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 0.7.6+repack0.
  * IMPORTANT NOTICE: created debian/README.source to explain about new
    upstream version.
  * Migrated Deb-Src to 3.0 format.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added ${misc:Depends} variable to Depends field.
      - Added a Homepage field pointing to web.archive.org.
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Changed from perlmagick to libimage-magick-perl in Depends field.
        Thanks to Martin Michlmayr <tbm@hp.com>. (Closes: #789237)
      - Created VCS fields.
      - Improved the long description.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/docs: removed webcamd.conf, already in debian/examples.
    (Closes: #739149)
  * debian/rules: migrated to new (reduced) format.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform a trivial CI tests.
  * debian/watch: added a fake site to explain about the current status of the
    original upstream homepage.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 05 Apr 2020 17:14:23 -0300

webcamd (0.7.6-6) unstable; urgency=medium

  * QA upload.
  * Re-upload as source only to target testing.
  * Set Debian QA Group as maintainer. (See #928540)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 01 Sep 2019 15:00:29 -0300

webcamd (0.7.6-5.2) unstable; urgency=medium

  * Non-maintainer upload
  * Fix another FTBFS, patch by Santiago Vila. Closes: #849261

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 25 Dec 2016 13:55:37 +0100

webcamd (0.7.6-5.1) unstable; urgency=medium

  * Non-maintainer upload
  * Declare debhelper compat level, bump to 10. Closes: #817720

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 18 Dec 2016 23:29:31 +0100

webcamd (0.7.6-5) unstable; urgency=low

  * Move debhelper to build-dep
  * Bump standards version

 -- Julien Danjou <acid@debian.org>  Tue,  4 Jul 2006 12:08:26 +0200

webcamd (0.7.6-4) unstable; urgency=low

  * /usr/doc transition (Closes: #359597)
  * Bump standard version
  * Change dh_compat to 4 and update build dep
  * Update FSF address

 -- Julien Danjou <acid@debian.org>  Fri,  7 Apr 2006 15:24:56 +0200

webcamd (0.7.6-3) unstable; urgency=low

  * Description updated (Closes: #144241)

 -- Julien Danjou <acid@debian.org>  Tue, 23 Apr 2002 18:59:56 +0200

webcamd (0.7.6-2) unstable; urgency=low

  * Some fix in manpages

 -- Julien Danjou <acid@debian.org>  Sun, 21 Apr 2002 08:17:06 +0200

webcamd (0.7.6-1) unstable; urgency=low

  * New upstream release.
  * Now in Debian (Closes: Bug#143343)

 -- Julien Danjou <acid@debian.org>  Wed, 17 Apr 2002 23:09:47 +0200

webcamd (0.7.5-2) unstable; urgency=low

  * New man page for webcamd-setup

 -- Julien Danjou <acid@hno3.org>  Sat, 23 Feb 2002 23:18:15 +0100

webcamd (0.7.5-1) unstable; urgency=low

  * New Upstream Release.

 -- Julien Danjou <acid@hno3.org>  Sat, 23 Feb 2002 17:49:58 +0100

webcamd (0.7.4-1) unstable; urgency=low

  * Initial Release.

 -- Julien Danjou <acid@hno3.org>  Wed, 20 Feb 2002 23:55:55 +0100
